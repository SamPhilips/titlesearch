﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EATurnerTilteSearch
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TitleDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (sender != null && ((sender as DataGrid).SelectedItem !=null))
            {
                int groupName = ((sender as DataGrid).SelectedItem as Common.TitleDetails).TitleIdentity;
                (TitleDetailsGrid.DataContext as EATurnerTilteSearch.ViewModel.TitleSearchViewModel).GetTitleDetais(groupName);
            }
        }
    }
}

﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace EATurnerTilteSearch.Helpers
{
    public class SearchCommand :ICommand
    {
        private Action<string> _stringAction;

        public SearchCommand(Action<string> stringAction)
        {
            this._stringAction = stringAction;
        }


        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged;

        public void Execute(object parameter)
        {
            if (parameter == null)
                parameter = string.Empty;

            _stringAction((parameter.ToString()));
        }
    }
}

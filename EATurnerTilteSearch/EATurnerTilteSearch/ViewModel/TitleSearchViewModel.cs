﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using EATurnerTilteSearch.Helpers;
using System.Collections.ObjectModel;
using System.Linq;
using Common;
using Model;


namespace EATurnerTilteSearch.ViewModel
{
    public class TitleSearchViewModel:ViewModelBase
    {
      
        // constructors to intialise the methods
        public TitleSearchViewModel()
        {
            Appstart();    
        }
        private void Appstart()
        {
            // SearchText = "Help";
            _storyDelegate = new StoryLineDelegate(SetStoryDescriptionLanguage);
            storyLineEvent += _storyDelegate;
            TitleCollection = new MethodImplemetationWrapper().GetLatestTitles();
            //  TitleDetail = new TitleRepository().returnTitleDetials();
            _searchCommand = new SearchCommand(GetFilteredCollection);
 
        }
        // define the properties that will be used
        /*
         * for the Title search veiw, we can divide it into 3 regions and each region will be filled by an appropriate property
         * Region 1 : the text box and the search button 
         * Region 2 : the datagrid that will hold the results from the title search
         * Region 3 : the title detials part.
         */
        private string _searchText;
        private ObservableCollection<TitleDetails> _titleCollection;
        private ExtentedTitleDetails _titleDetial;
        private ICommand _searchCommand;
        private string _storylineType;
        private Common.StoryLine _storyline;

        private delegate void StoryLineDelegate(string storytype);

        private event StoryLineDelegate storyLineEvent;

        private StoryLineDelegate _storyDelegate;

        

        // Text that is used to search the titles
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                if(_searchText != value)
                {
                    _searchText = value;
                    OnPropertyChanged("SearchText");
                }
            }
        }

        /// <summary>
        /// Used to bind the title collection 
        /// </summary>
        public ObservableCollection<TitleDetails> TitleCollection
        {
            get { return _titleCollection; }
            set
            {

                _titleCollection = value;
                OnPropertyChanged("TitleCollection");

            }
        }


       /// <summary>
       /// used to bind the extensive title details 
       /// </summary>
        public ExtentedTitleDetails TitleDetail
        {
            get { return _titleDetial; }
            set
            {
                _titleDetial = value;
                OnPropertyChanged("TitleDetail");
                
            }
        }

        public string SelectedStoryLineType
        {
            get { return _storylineType; }
            set
            {
                _storylineType = value;
                OnPropertyChanged("SelectedStoryLineType");
                storyLineEvent(value);
            }
 
        }

        public Common.StoryLine SelectedStoryLine
        {
            get { return _storyline; }
            set
            {
                _storyline = value;
                OnPropertyChanged("SelectedStoryLine");
                // Raise an event to  bind the storyline detials

            }

        }

      





        public ICommand SearchCommand
        {
            get { return _searchCommand; }
        }

        public void GetFilteredCollection(String searchText)
        {
            IMethodDetails details = new MethodImplemetationWrapper();

            TitleCollection = details.GetFilteredTitles(searchText);
        }

        public void GetTitleDetais(int TitleIdentity)
        {
            IMethodDetails details = new MethodImplemetationWrapper();

            TitleDetail = details.GetTitleDetails(TitleIdentity);
            if(TitleDetail.StoryLineDetails != null && TitleDetail.StoryLineDetails.Count > 0)
            {
                SelectedStoryLine = TitleDetail.StoryLineDetails[0];
                SelectedStoryLineType = SelectedStoryLine.StoryType;

            }
           
        }

        private void SetStoryDescriptionLanguage(string storytype)
        {
            SelectedStoryLine = TitleDetail.StoryLineDetails.Where((Common.StoryLine S) => S.StoryType == storytype).FirstOrDefault();
                                
        }

       

        // define the commands 


    }
}

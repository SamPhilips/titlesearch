﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Common
{
    /// <summary>
    /// the class contains the extended details of the title.
    /// </summary>
   public class ExtentedTitleDetails : TitleDetails
    {
       private string _genere;
       private ObservableCollection<StoryLine> _storylineDetials;
       private bool _isStoryLineAvailable;
       private ObservableCollection<Awards> _awarddetails;
       private bool _areAwardDetailsAvailable;
       private ObservableCollection<CastDetails> _castdetails;
       private bool _isCastDetailsAvailable;

       public ExtentedTitleDetails()
       {
           AwardDetails = new ObservableCollection<Awards>();
           CastDetails = new ObservableCollection<CastDetails>();
       }

       /// <summary>
       /// Holds the genere to which the title belongs to.
       /// </summary>
       public string Genre
       {
           get { return _genere; }

           set { _genere = value; }
       }

       /// <summary>
       /// Holds the story line details of the title.
       /// </summary>
       public ObservableCollection<StoryLine> StoryLineDetails
       {
           get { return _storylineDetials; }

           set { _storylineDetials = value; }
       }

       /// <summary>
       /// Holds the flag if  story line details are available.
       /// </summary>
       public bool ISStoryLineAvailable
       {
           get { return _isStoryLineAvailable; }

           set { _isStoryLineAvailable = value; }
       }


       /// <summary>
       /// Holds the  details of the cast in the title.
       /// </summary>
       public ObservableCollection<CastDetails> CastDetails
       {
           get { return _castdetails; }

           set { _castdetails = value; }
       }

       /// <summary>
       /// Holds the flag if  cast details are available.
       /// </summary>
       public bool ISCastAvailable
       {
           get { return _isCastDetailsAvailable; }

           set { _isCastDetailsAvailable = value; }
       }

       /// <summary>
       /// Holds the  details of the awards for the title.
       /// </summary>
       public ObservableCollection<Awards> AwardDetails 
       {
           get { return _awarddetails; }

           set { _awarddetails = value; }
       }


       /// <summary>
       /// Holds the flag if  Award details are available.
       /// </summary>
       public bool AreAwardsAvailable
       {
           get { return _areAwardDetailsAvailable; }

           set { _areAwardDetailsAvailable = value; }
       }


       

    }
}

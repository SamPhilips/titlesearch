﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Common
{
    public interface IMethodDetails
    {
         ObservableCollection<TitleDetails> GetFilteredTitles(string TitleName);
         ObservableCollection<TitleDetails> GetLatestTitles();
         ExtentedTitleDetails GetTitleDetails(int TitleIdentity);
    
    }
}

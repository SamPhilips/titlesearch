﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// Holds the Award information for a title.
    /// </summary>
    public class Awards
    {
        #region Private members

        private string _awardWon;
        private string _award;
        private string _awardCompany;
        private int? _awardYear;

        #endregion

        #region Public properties

        /// <summary>
        /// Holds if the award was won or nominated
        /// </summary>
        public string AwardWon
        {
            get { return _awardWon; }

            set { _awardWon = value; }
        }
        /// <summary>
        /// Holds the award which was provided/Nominated
        /// </summary>
        public string Award
        {
            get { return _award; }

            set { _award = value; }
        }
        /// <summary>
        /// Holds the Company which provided the award
        /// </summary>
        public string AwardCompany
        {
            get { return _awardCompany; }

            set { _awardCompany = value; }
        }
        /// <summary>
        /// Holds the year which the award was given/Nominated
        /// </summary>
        public int? AwardYear
        {
            get { return _awardYear; }

            set { _awardYear = value; }
        }

        #endregion
    }
}

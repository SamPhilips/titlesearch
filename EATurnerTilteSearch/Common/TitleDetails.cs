﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class TitleDetails
    {
        #region Private members
        
        private int _titleIdentity;
        private string _titleName;
        private int? _releaseDate;
        private bool _hasAwards;
        private string _originalTitleName;
        private string _titleLanguage;

        #endregion

        #region Public properties

        /// <summary>
        /// Holds the title Identity for the titles. Not usually displayed in screen 
        /// </summary>
        public int TitleIdentity
        {
            get { return _titleIdentity; }

            set { _titleIdentity = value; }
        }
        /// <summary>
        /// Holds the title Names 
        /// </summary>
        public string TitleName
        {
            get { return _titleName; }

            set { _titleName = value; }
        }
        /// <summary>
        /// Holds the year in which the film was released
        /// </summary>
        public int? ReleaseDate
        {
            get { return _releaseDate; }

            set { _releaseDate = value; }
        }
        /// <summary>
        /// Flag to hold if the film has won any awards
        /// </summary>
        public bool HasAwards
        {
            get { return _hasAwards; }

            set { _hasAwards = value; }
        }
        /// <summary>
        /// Holds the Actual Title of the Movie
        /// </summary>
        public string OriginalTitleName
        {
            get { return _originalTitleName; }

            set { _originalTitleName = value; }
        }
        /// <summary>
        /// Holds the title Identity for the titles. Not usually displayed in screen 
        /// </summary>
        public string TitleLanguage
        {
            get { return _titleLanguage; }

            set { _titleLanguage = value; }
        }

        #endregion
    }
}

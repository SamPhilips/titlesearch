﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    /// <summary>
    /// Holds the details of the cast involved in the movie.
    /// </summary>
    public class CastDetails
    {
        #region Private members

        private string _onOrOffScreen;
        private string _participantName;
        private string _roleType;
        private bool _isAKeyrole;

        #endregion

        #region Public properties

        /// <summary>
        /// Holds the details if the person was oncamera or off camera.
        /// </summary>
        public string OnOrOffScreen
        {
            get { return _onOrOffScreen; }

            set { _onOrOffScreen = value; }
        }
        /// <summary>
        /// Holds the Name of the participant
        /// </summary>
        public string ParticipantName
        {
            get { return _participantName; }

            set { _participantName = value; }
        }
        /// <summary>
        /// Holds the role type of the participant
        /// </summary>
        public string RoleType
        {
            get { return _roleType; }

            set { _roleType = value; }
        }
        /// <summary>
        /// Holds the boolean value wheather the participant has played a key role
        /// </summary>
        public bool IsAKeyRole
        {
            get { return _isAKeyrole; }

            set { _isAKeyrole = value; }
        }

        #endregion
    }
}

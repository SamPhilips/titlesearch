﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class StoryLine
    {

        #region Private members

        private string _storyType;
        private string _storyLanguage;
        private string _storyDescription;

        #endregion

        #region Public properties


        /// <summary>
        /// Holds the type to which the story belongs to 
        /// </summary>
        public string StoryType
        {
            get { return _storyType; }

            set { _storyType = value; }
        }
        /// <summary>
        /// Holds the language of the story
        /// </summary>
        public string StoryLanguage
        {
            get { return _storyLanguage; }

            set { _storyLanguage = value; }
        }
        /// <summary>
        /// Holds the detailed description of the story 
        /// </summary>
        public string StoryDescription
        {
            get { return _storyDescription; }

            set { _storyDescription = value; }
        }

        #endregion

    }
}

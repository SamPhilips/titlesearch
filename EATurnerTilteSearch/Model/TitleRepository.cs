﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Common;

namespace Model
{
    /// <summary>
    /// Populates the class with the model data so that can be consumed during design;
    /// </summary>
    public class TitleRepository
    {
        public ObservableCollection<TitleDetails> ReturnTitleCollection()
        {
            return new ObservableCollection<TitleDetails>()
            {
                new TitleDetails {  TitleName = "Lord of the rings"
                                  , TitleIdentity = 1
                                  , TitleLanguage = "English"
                                  , HasAwards = true
                                  , OriginalTitleName = "Lord of the Rings"
                                  , ReleaseDate = 2013
                                 },

                new TitleDetails {  TitleName = "Titanic"
                                  , TitleIdentity = 2
                                  , TitleLanguage = "English"
                                  , HasAwards = false
                                  , OriginalTitleName = "Titanic"
                                  , ReleaseDate =2010
                                 },
                 new TitleDetails {  TitleName = "Rio The bird story"
                                  , TitleIdentity = 3
                                  , TitleLanguage = "English"
                                  , HasAwards = false
                                  , OriginalTitleName = "Rio The bird story"
                                  , ReleaseDate = 2013
                                 }
            };

        }

        /* write a method that will produce the extended title details.
         * witout story line
         * without awards
         * without cast 
         * without genere
        */
        // the belwow method will get the detials with the title detials class

        public ExtentedTitleDetails returnTitleDetials()
        {
            return new ExtentedTitleDetails()
            {
                TitleName = "Rio The bird story"
               ,
                TitleIdentity = 3
               ,
                TitleLanguage = "English"
               ,
                HasAwards = false
               ,
                OriginalTitleName = "Rio The bird story"
               ,
                ReleaseDate = null
               ,
                AreAwardsAvailable = true
               ,
                ISCastAvailable = true
               ,
                ISStoryLineAvailable = true
               ,
                Genre = " Adventure, Kids, Animation"
               ,
                CastDetails = new ObservableCollection<CastDetails>()
               { new CastDetails(){OnOrOffScreen = "Onscreen"
                                  ,IsAKeyRole = true
                                  ,ParticipantName = "Cameron Diaz"
                                  ,RoleType = "Felmale Lead(Voice)" }

                , new CastDetails(){OnOrOffScreen = "Onscreen"
                                  ,IsAKeyRole = false
                                  ,ParticipantName = "Ton Cruise"
                                  ,RoleType = "Male Lead (Voice)" }

                , new CastDetails(){OnOrOffScreen = "Offscreen"
                                  ,IsAKeyRole = true
                                  ,ParticipantName = "Stephien SpielBerg"
                                  ,RoleType = "Director" }
               
               }
               ,
                AwardDetails = new ObservableCollection<Awards>()
               {
                   new Awards(){ Award = "Oscars"
                               , AwardCompany = "Oscars"
                               , AwardWon = "Nominated"
                               , AwardYear = 2013
                               }
                 , new Awards(){ Award = "Golden Globe"
                               , AwardCompany = "United Kingdom"
                               , AwardWon = "Winner"
                               , AwardYear = 2013
                               }
               }
               ,
                StoryLineDetails = new ObservableCollection<Common.StoryLine>()
               {
                   new Common.StoryLine()
                    {
                        StoryLanguage = "English"
                       ,
                        StoryType = "When Blu, a domesticated macaw from small-town Minnesota, meets the fiercely independent Jewel, he takes off on an adventure to Rio de Janeiro with this bird ..."
                       ,
                        StoryDescription = "In Rio de Janeiro, baby macaw, Blu, is captured by dealers and smuggled to the USA."
                                          + "While driving through Moose Lake, Minnesota, the truck that is transporting Blu accidentally"
                                          + "drops Blu's box on the road. A girl, Linda, finds the bird and raises him with love. "
                                          + "Fifteen years later, Blu is a domesticated and intelligent bird that does not fly and lives "
                                          + "a comfortable life with bookshop owner Linda. Out of the blue, clumsy Brazilian ornithologist,"
                                          + "Tulio, visits Linda and explains that Blu is the last male of his species, and he has a female called Jewel"
                                          + "in Rio de Janeiro. He invites Linda to bring Blu to Rio so that he and Jewel can save their species."
                                          + "Linda travels with Blu and Tulio to Rio de Janeiro and they leave Blu and Jewel in a large cage in the institute where Tulio works"
                                          + ". While they are having dinner, smugglers break into the institute and steal Blu and Jewel to sell them."
                                          + "Linda and Tulio look everywhere for Blu, who is chained to Jewel and hidden in a slum. Meanwhile, Jewel ... "
                    }
               }







            };
        
        }

        public ExtentedTitleDetails returnTitleDetialswithnoDetails()
        {
            return new ExtentedTitleDetails()
            {
                TitleName = "Titanic"
                                  ,
                TitleIdentity = 2
                                  ,
                TitleLanguage = "English"
                                  ,
                HasAwards = false
                                  ,
                OriginalTitleName = "Titanic"
                                  ,
                ReleaseDate = 2010
               ,
                AreAwardsAvailable = false
               ,
                ISCastAvailable = false
               ,
                ISStoryLineAvailable = false
               ,
                Genre = " Romance, Drama"
               ,
                CastDetails = new ObservableCollection<CastDetails>()
               { new CastDetails(){OnOrOffScreen = "Not Available"
                                  ,IsAKeyRole = false
                                  ,ParticipantName = "Not Available"
                                  ,RoleType = "Not Available" }
               }
               ,
                AwardDetails = new ObservableCollection<Awards>()
               {
                   new Awards(){ Award = "Not Available"
                               , AwardCompany = "Not Available"
                               , AwardWon = "Not Available"
                               , AwardYear = null
                               }
               }
               ,
               StoryLineDetails = new ObservableCollection<Common.StoryLine>()
               {
                 new Common.StoryLine()
                {
                    StoryLanguage = "Not Available"
                   ,
                    StoryType = "Not Available"
                   ,
                    StoryDescription = "Not Available"
                }
               }







            };

        }

    }

}

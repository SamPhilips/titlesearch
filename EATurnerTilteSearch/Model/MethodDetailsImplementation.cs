﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using Common;
using System.Configuration;

namespace Model
{
    /// <summary>
    /// this class will be called to handle the methods defined in the interface
    /// </summary>
    public class MethodDetailsImplementation
    {
        private string _connectionString;
        private readonly string _defaultString;

        // the conenction to the entity framework is achieved by creating the object using the connection string.
        public MethodDetailsImplementation()
        {
            _connectionString = ConfigurationManager.AppSettings["ConnectionString"]; ;
            _defaultString = ConfigurationManager.AppSettings["DefaultValue"];
        }

        internal ObservableCollection<TitleDetails> GetFilteredTitles(string TitleName)
        {

            try
            {
                ObservableCollection<TitleDetails> TitleCollection = null;

                if (!TitleName.Trim().Equals(string.Empty))
                {
                    using (TitlesEntities context = new TitlesEntities(_connectionString))
                    {
                        var TitleEnumarble = from source in context.Titles
                                             where source.TitleName.Contains(TitleName)
                                             orderby source.TitleNameSortable, source.ProcessedDateTimeUTC descending
                                             select new TitleDetails
                                             {
                                                 TitleIdentity = source.TitleId,
                                                 TitleName = source.TitleName,
                                                 ReleaseDate = source.ReleaseYear,
                                                 HasAwards = (source.Awards).Count > 0 ? true : false
                                             };

                        TitleEnumarble = TitleEnumarble.Distinct();

                        TitleCollection = new ObservableCollection<TitleDetails>(TitleEnumarble);
                    }
                    return TitleCollection;
                }
                else
                {
                    return new ObservableCollection<TitleDetails>();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        internal ObservableCollection<TitleDetails> GetLatestTitles()
        {
            try
            {
                ObservableCollection<TitleDetails> TitleCollection = null;

                using (TitlesEntities context = new TitlesEntities(_connectionString))
                {
                    var TitleEnumarble = from source in context.Titles
                                         orderby source.ProcessedDateTimeUTC descending
                                         select new TitleDetails
                                         {
                                             TitleIdentity = source.TitleId,
                                             TitleName = source.TitleName,
                                             ReleaseDate = source.ReleaseYear,
                                             HasAwards = (source.Awards).Count > 0 ? true : false
                                         };

                    TitleEnumarble = TitleEnumarble.Distinct();

                    TitleCollection = new ObservableCollection<TitleDetails>(TitleEnumarble);
                }
                return TitleCollection;
            }
            catch (Exception)
            {
                throw;
            }
        }

        internal ExtentedTitleDetails GetTitleDetails(int TitleIdentity)
        {

            try
            {
                ExtentedTitleDetails TitleCollection = null;
                string genreVariable = string.Empty;
                Common.StoryLine storylineVariable = new Common.StoryLine();

                if (TitleIdentity != 0)
                {
                    using (TitlesEntities context = new TitlesEntities(_connectionString))
                    {
                        var TitleEnumarble = from source in context.Titles
                                             where source.TitleId == TitleIdentity
                                             select new ExtentedTitleDetails
                                             {
                                                 TitleIdentity = source.TitleId,
                                                 TitleName = source.TitleName,
                                                 ReleaseDate = source.ReleaseYear,
                                                 HasAwards = (source.Awards).Count > 0 ? true : false,
                                                 OriginalTitleName = source.OtherNames.FirstOrDefault().TitleName,
                                                 TitleLanguage = source.OtherNames.FirstOrDefault().TitleNameLanguage

                                             };

                        //Genere

                        var genreEnumarable = from source in context.TitleGenres
                                               where source.TitleId == TitleIdentity
                                               select source.Genre.Name;


                        if (genreEnumarable != null && genreEnumarable.Count() > 0)
                            genreVariable = String.Join(",", genreEnumarable);
                        else
                            genreVariable = _defaultString;

                        //Story Line details

                        var StorylineEnumarable = from source in context.StoryLines
                                                  where source.TitleId == TitleIdentity
                                                  select new Common.StoryLine
                                                  {
                                                      StoryType = source.Type != null ? source.Type : _defaultString,
                                                      StoryLanguage = source.Language != null ? source.Language : _defaultString,
                                                      StoryDescription = source.Description != null ? source.Description : _defaultString
                                                  };


                        //Cast details

                        var CastEnumarable = from source in context.TitleParticipants
                                             where source.TitleId == TitleIdentity
                                             select new CastDetails
                                             {
                                                 IsAKeyRole = source.IsKey != null ? source.IsKey : false,
                                                 OnOrOffScreen = source.IsOnScreen != null ? (source.IsOnScreen == true ? "OnScreen" : "OffScreen") : _defaultString,
                                                 RoleType = source.RoleType != null ? source.RoleType : _defaultString,
                                                 ParticipantName = source.Participant.Name != null ? source.Participant.Name : _defaultString
                                             };


                        //Awards

                        var awardsEnumarable = from source in context.Awards
                                               where source.TitleId == TitleIdentity
                                               select new Awards
                                               {
                                                   Award = source.Award1 != null ? source.Award1 : _defaultString,
                                                   AwardCompany = source.AwardCompany != null ? source.AwardCompany : _defaultString,
                                                   AwardWon = source.AwardWon.HasValue ? (source.AwardWon.Value == true ? "Award Won" : "Nominated") : _defaultString,
                                                   AwardYear = source.AwardYear
                                               };

                        TitleCollection = new ExtentedTitleDetails()
                        {
                            TitleIdentity = TitleEnumarble.FirstOrDefault().TitleIdentity,
                            TitleName = TitleEnumarble.FirstOrDefault().TitleName,
                            ReleaseDate = TitleEnumarble.FirstOrDefault().ReleaseDate,
                            HasAwards = TitleEnumarble.FirstOrDefault().HasAwards,
                            StoryLineDetails = new ObservableCollection<Common.StoryLine>( StorylineEnumarable),
                            ISStoryLineAvailable = (StorylineEnumarable != null) ? true : false,
                            CastDetails = new ObservableCollection<CastDetails>(CastEnumarable),
                            ISCastAvailable = (CastEnumarable != null && CastEnumarable.Count() > 0) ? true : false,
                            Genre = genreVariable,
                            AwardDetails = new ObservableCollection<Awards>(awardsEnumarable),
                            AreAwardsAvailable = (awardsEnumarable != null && awardsEnumarable.Count() > 0) ? true : false,
                            OriginalTitleName = TitleEnumarble.FirstOrDefault().OriginalTitleName,
                            TitleLanguage = TitleEnumarble.FirstOrDefault().TitleLanguage

                        };

                       
                    }
                    return TitleCollection;
                }
                else
                {
                    return TitleCollection;
                }
            }
            catch (Exception)
            {
                throw;
            }


            // creating an instance of the Entity class
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Collections.ObjectModel;
namespace Model
{
    public class MethodImplemetationWrapper : IMethodDetails
    {
        private MethodDetailsImplementation methodImplementationInstance;

        public MethodImplemetationWrapper()
        {
            methodImplementationInstance = new MethodDetailsImplementation();
        }


        // method for title repository
        // gets the filtered titles based on the Title Name
        public ObservableCollection<TitleDetails> GetFilteredTitles(string TitleName)
        {
            try
            {
                if (TitleName.Equals(string.Empty))
                    return methodImplementationInstance.GetLatestTitles();
                else
                    return methodImplementationInstance.GetFilteredTitles(TitleName);

            }
            catch
            {
                throw;

            }

        }

        //gets the recent titles
        public ObservableCollection<TitleDetails> GetLatestTitles()
        {
            try
            {
                return methodImplementationInstance.GetLatestTitles();

            }
            catch
            {
                throw;

            }
        }

        //Gets the titleID details 
        public ExtentedTitleDetails GetTitleDetails(int TitleIdentity)
        {
            try
            {
                return methodImplementationInstance.GetTitleDetails(TitleIdentity);

            }
            catch
            {
                throw;

            }
           
        }
    }
}
